module AbstractMachine.FixPoint
(
  PFunction
  , fix
  , bottom
  , id
)
where

import Control.Applicative
import Prelude hiding (id)

-- Partial functions from type a to type b are modeled by functions that go from
-- a to Maybe b. When the function doesn't terminate Nothing is retirned, otherwise Just is returned.
type PFunction a b = a -> Maybe b

-- Given a partial function, returns its least fix point.
fix :: (PFunction a b -> PFunction a b) -> PFunction a b
fix func = chainLub (iterate func bottom)

-- Returns the lub of a list of partial functions that are in ascending order.
chainLub :: [PFunction a b] -> PFunction a b
chainLub chain val = foldr1 (<|>) (map (\f -> f val) chain)

-- Represent the function that returns always Nothing i.e it models the bottom function.
bottom :: PFunction a b
bottom = const Nothing

-- Represent the identity function.
id :: PFunction a a
id = Just
