module AbstractMachine.StmExpression (
  stmDSem
)
where

import Control.Monad
import Prelude hiding(id)

import AbstractMachine.Store
import qualified Parser.WhileAST as AST (Expr(..))
import Parser.WhileParser (Name)
import AbstractMachine.ArthExpression
import AbstractMachine.BoolExpression
import AbstractMachine.FixPoint

-- An alias for partial functions between states.
type StatePFunc = PFunction State State

-- Return the denotational semantic of the given expression.
stmDSem :: AST.Expr Name -> StatePFunc

-- Semantic of the assignment expression. Evaluates the arithmetic expression and 
-- then assign the rsult to the variable.
stmDSem (AST.Asgn var aexpr) = \env -> Just $ save var (arthDSem aexpr env)  env

-- Semantic of the skip expression.
stmDSem AST.Skip = id

-- Semantic of the composition of two espression. Evaluates the first expression
-- and if it terminates, meaning Nothing is not returned, evaluate the second one.
stmDSem (AST.Comp expr1 expr2)  = stmDSem expr1 >=> stmDSem expr2

-- Semantic of the conditional expression. 
stmDSem (AST.Cond bexpr expr1 expr2) = cond (boolDSem bexpr) (stmDSem expr1) (stmDSem expr2)

-- Semantic of the while expression. Construct hte funcional related to the while expression
-- and the returns its least fix point.
stmDSem (AST.While bexpr expr) = fix func
  where
    func :: StatePFunc -> StatePFunc
    func g = cond (boolDSem bexpr) (stmDSem expr >=> g) id

-- The 'Cond' function.
cond :: (State-> Bool) -> StatePFunc -> StatePFunc -> StatePFunc
cond bSem stm1Sem stm2Sem env = if bSem env
                                   then stm1Sem env
                                   else stm2Sem env
