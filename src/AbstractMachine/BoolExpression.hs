module AbstractMachine.BoolExpression
where

import AbstractMachine.Store
import qualified Parser.WhileAST as AST (BExpr(..))
import Parser.WhileParser (Name)
import AbstractMachine.ArthExpression

-- Return the denotational semantic of the given boolean expression.
boolDSem :: AST.BExpr Name -> State -> Bool

-- Semantic of the 'True' litteral.
boolDSem AST.True _ = True

-- Semantic of the 'False' litteral.
boolDSem AST.False _ = False

-- Semantic of the equality comparison. Firstly evaluates both
-- the arithmetic expressions and then compare thei values.
boolDSem (AST.Eq aexpr1 aexpr2) env = a1 == a2
  where
    a1 = arthDSem aexpr1 env
    a2 = arthDSem aexpr2 env

-- Semantic of the inequality comparison. Firstly evaluates both
-- the arithmetic expressions and then compare thei values.
boolDSem (AST.Le aexpr1 aexpr2) env = a1 <= a2
  where
    a1 = arthDSem aexpr1 env
    a2 = arthDSem aexpr2 env

-- Semantic of the negation.
boolDSem (AST.Not bexpr) env = not b
  where
    b = boolDSem bexpr env

-- Semantic of the conjuction.
boolDSem (AST.And bexpr1 bexpr2) env = b1 && b2
  where
    b1 = boolDSem bexpr1 env
    b2 = boolDSem bexpr2 env

