module AbstractMachine.ArthExpression
where

import Prelude hiding(read)

import AbstractMachine.Store
import Parser.WhileAST (AExpr(..))
import Parser.WhileParser (Name)

-- Return the denotational semantic of the given arithmetic expression.
arthDSem :: AExpr Name -> State -> Int

-- Semantic of a variable, simply read its value in the supplied state.
arthDSem (Var x) env = read x env

-- Semantic of the a number.
arthDSem (Val n) _= n

-- Semantic of the opposite of an arithmentic operation. 
arthDSem (Neg aexpr) env = -(arthDSem aexpr env)

-- Semantic of addition.
arthDSem (Plus aexpr1 aexpr2) env = a1 + a2
  where
  a1 = arthDSem aexpr1 env
  a2 = arthDSem aexpr2 env

-- Semantic of multiplication.
arthDSem (Multi aexpr1 aexpr2) env = a1 * a2
  where
  a1 = arthDSem aexpr1 env
  a2 = arthDSem aexpr2 env

-- Semantic of subtraction.
arthDSem (Minus aexpr1 aexpr2) env = a1 - a2
  where
  a1 = arthDSem aexpr1 env
  a2 = arthDSem aexpr2 env

