module AbstractMachine.Store
where

import qualified Data.Map.Strict as M

-- The state is represented by a dictionary from strings(variables) and integers.
type State = M.Map String Int

-- A variable is simply a string.
type Variable = String

-- Return an empty state.
emptyMem :: State
emptyMem = M.empty

-- Return the value of the variable in the given state defaulting to 0 if is absent.
read :: Variable -> State  -> Int
read = M.findWithDefault 0

-- Return a new state in which the value of the variable is updated. 
save :: Variable -> Int -> State -> State
save = M.insert
