module AbstractMachine.Machine
where

import Data.Maybe

import Parser.WhileAST (Expr (..))
import Parser.WhileParser (Name)
import AbstractMachine.Store

import AbstractMachine.StmExpression

-- Interprets the given AST in the given state.
exec :: Expr Name -> State -> State
exec prog = fromJust . stmDSem prog

