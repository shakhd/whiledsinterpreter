module Parser.WhileAST (
  Expr (..),
  AExpr (..),
  BExpr (..),
  Program
)where

-- Datatypes for the AST. Its type is parametric with respect of the type of variable.

-- AST of a program is simply the one of an expression.
type Program a = Expr a

-- AST for arithmetic expressions.
data AExpr a = Val Int -- An integer.
             | Var a -- A variable.
             | Neg (AExpr a) -- The opposite of an arithemtic expression.
             | Plus (AExpr a) (AExpr a) -- Addition.
             | Multi (AExpr a) (AExpr a) -- Subtraction.
             | Minus (AExpr a) (AExpr a) -- Multiplication.
             deriving(Show)

-- AST for boolean expressions.
data BExpr a = True -- True litteral.
             | False -- False litteral. 
             | Eq (AExpr a) (AExpr a) -- Equality comparison.
             | Le (AExpr a) (AExpr a) -- Inequality comparison.
             | Not (BExpr a) -- Negation.
             | And (BExpr a) (BExpr a) -- Conjuction.
             deriving(Show)

-- AST for expressions.
data Expr a = Asgn a (AExpr a) -- Assignment
            | Skip -- Skip.
            | Comp (Expr a) (Expr a) -- Composition.
            | Cond (BExpr a) (Expr a) (Expr a) -- Conditional 
            | While (BExpr a) (Expr a) -- While
            deriving(Show)
