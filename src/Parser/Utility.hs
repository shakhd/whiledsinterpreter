module Parser.Utility
where 

import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

type Parser = Parsec Void String

-- Parse spaces, comments and multilines comments.
sc :: Parser ()
sc = L.space space1 lineCmnt blockCmnt
  where
    lineCmnt  = L.skipLineComment "//"
    blockCmnt = L.skipBlockComment "/*" "*/"

-- Remove the trailing spaces
lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

-- Parse the given string.
symbol :: String -> Parser String
symbol = L.symbol sc

-- Parse the expression in parentesis.
parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")

-- Parse an integer.
integer :: Parser Int
integer = lexeme L.decimal

--Parse a semicolon.
semi :: Parser String
semi = symbol ";"

--Parse the given string as a reserved word.
rword :: String -> Parser ()
rword w = (lexeme . try) (string w *> notFollowedBy alphaNumChar)

-- Reserved words
rws :: [String] 
rws = ["true", "false", "skip", "case", "if", "then", "else", "while", "do"]

-- Parse an identifier.
identifier :: Parser String
identifier = (lexeme . try) (p >>= check)
  where
    p       = (:) <$> letterChar <*> many alphaNumChar
    check x = if x `elem` rws
                then fail $ "keyword " ++ show x ++ " cannot be an identifier"
                else return x

