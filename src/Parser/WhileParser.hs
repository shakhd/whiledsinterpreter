{-# OPTIONS_GHC -fno-warn-unused-do-bind #-}
module Parser.WhileParser where

import Text.Megaparsec
import Text.Megaparsec.Expr
import Text.Megaparsec.Char
import Parser.Utility
import Parser.WhileAST
import Prelude hiding (True, False)

-- The Parser is created following the one created in this tutorial
-- https://markkarpov.com/megaparsec/parsing-simple-imperative-language.html
-- which is very similar to the parser for the While language.

-- The type of the variable.
type Name = String

-- Parse the given program.
parseProg :: String -> Program Name
parseProg prog = case (parse whileParser "" prog) of
  Left err -> error (parseErrorPretty err)
  Right e -> e

-- Parser of a While program.
whileParser :: Parser (Program Name)
whileParser = between sc eof compExpr

-- Parse composition of expressions.
compExpr :: Parser (Expr Name)
compExpr = f <$> sepBy1 simpleExpr semi
  where
    f expr = if length expr == 1 
      then head expr 
      else foldr1 Comp expr

-- Parse a single While expression.
simpleExpr :: Parser (Expr Name)
simpleExpr = ifExpr
  <|> whileExpr
  <|> skipExpr
  <|> (assignExpr <?> "Assignment")
  <|> (parens compExpr <?> "")

-- Parse the conditional expression.
ifExpr :: Parser (Expr Name)
ifExpr = do
  rword "if"
  cond  <- bExpr
  rword "then"
  thenExpr <- compExpr
  rword "else"
  elseExpr <- compExpr
  return (Cond cond thenExpr elseExpr)

-- Parse the while expression.
whileExpr :: Parser (Expr Name)
whileExpr = do
  rword "while"
  cond <- bExpr 
  rword "do"
  expr <- compExpr
  return (While cond expr)

-- Parse the assignment expression-
assignExpr :: Parser (Expr Name)
assignExpr = do
  var  <- identifier
  (symbol ":=")
  aexpr <- aExpr
  return (Asgn var aexpr)

-- Parse the skip expression.
skipExpr :: Parser (Expr Name)
skipExpr = Skip <$ rword "skip"

-- Parse an arithmetic expression.
aExpr :: Parser (AExpr Name)
aExpr = (makeExprParser aTerm arthOp <?> "Arithmetic epxression")

-- Parse an boolean expression. 
bExpr :: Parser (BExpr Name)
bExpr = (makeExprParser bTerm boolOp <?> "Boolean Expression")

arthOp :: [[Operator Parser (AExpr Name)]]
arthOp =
  [ [Prefix (Neg <$ (symbol "-"*> lookAhead (noneOf ['*', '+' , '-'])))]
  , [ InfixL (Multi <$ (symbol "*" *> lookAhead (noneOf ['*', '+' , '-'])))]
  , [ InfixL (Plus  <$ (symbol "+" *> lookAhead (noneOf ['*', '+' , '-'])))
    , InfixL (Minus <$ (symbol "-" *> lookAhead (noneOf ['*', '+' , '-']))) ]
  ]

aTerm :: Parser (AExpr Name)
aTerm = (parens aExpr <?> "Arithmetic expression")
  <|> ((Var <$> identifier) <?> "Variable")
  <|> (Val <$> integer <?> "Number")

boolOp :: [[Operator Parser (BExpr Name)]]
boolOp =
  [ [Prefix (Not <$ rword "!") ]
  , [InfixL (And <$ rword "and")]
  ]

bTerm :: Parser (BExpr Name)
bTerm =  (parens bExpr <?> "Boolean Expression")
  <|> ((True  <$ rword "true") <?> "True")
  <|> ((False <$ rword "false") <?> "False")
  <|> (rExpr  <?> "Relational expression")

rExpr :: Parser (BExpr Name)
rExpr = do
  aexpr1 <- aExpr
  op <- relation
  aexpr2 <- aExpr
  return $ op aexpr1 aexpr2

relation :: Parser (AExpr Name -> AExpr Name -> BExpr Name) 
relation = (symbol "=" *> pure Eq) <|> (symbol "<=" *> pure Le)

