## While Language interpreter using denotaional semantic in Haskell.

### Prerequisites
* Haskell Platform (https://www.haskell.org/platform/)

### Install
* Oepen a shell window;
* Clone this project with git clone https://shakhd@bitbucket.org/shakhd/whiledsinterpreter.git
* Go to the newly created direcotory;
* and type `stack build` (this may take a while)

### Launch the interpreter
After the project is built we can launch the interpreter with the follwing command:

`stack exec -- WhileDS-exe flags`

where flags can be:

* *-f filePath* where *filepath* is path to a file containing a While program;
* *-p program* where *program* is a While program;

For example `stack exec -- WhileDS-exe -p "x := 4"`  
In addition to this two flags there is the optional `-s state` flag where *state* is a list of tuples
`(variable, value)` and allows us to specify an initial state. For example:  
`stack exec -- WhileDS-exe  -s '[("y", 1), ("z", 5)]' -p "x := 4"`