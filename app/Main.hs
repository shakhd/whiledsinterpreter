{-# LANGUAGE DeriveDataTypeable #-}

module Main where

import Parser.WhileParser
import qualified Data.Map.Strict as M
import AbstractMachine.Machine

import System.Console.CmdArgs

data Options = Options {
    filePath :: String
  , state :: String
  , str :: String
} deriving (Show, Data, Typeable)

options :: Options
options = Options {
    filePath = def &= typFile &= help "FilePath of the program"
  , state = "[]" &= name "s" &= help "The initial state"
  , str = def &= name "p" &= help "A While program"
}

main :: IO ()
main =  do
  op <- cmdArgs options
  if (filePath op) /= []
    then do
          prog <- readFile (filePath op)
          execProg (filter (/= '\n') prog) op
    else if (str op) /= []
      then execProg (str op) op
      else print "At least a filepath or an inline program must be specified."

execProg :: String -> Options ->IO ()
execProg prog op = do
  let finalState =  exec (parseProg prog) (M.fromList (read $ state op))
  putStrLn "Final state: "
  putStr $ M.foldrWithKey' (\ var val acc -> acc ++ var ++ " --> " ++ (show val) ++ "\n") "" finalState
